﻿#last parameter should be the same as service exe file name
Write-Host (Get-Item -Path ".\" -Verbose).FullName
$myPath = (Get-Item -Path ".\" -Verbose).FullName + "\WindowsServiceTemplate.exe"
Write-Host $myPath
New-Service -Name "WindowsServiceTemplate" -BinaryPathName $myPath -DisplayName "My Windows Service Template Project"
Pause