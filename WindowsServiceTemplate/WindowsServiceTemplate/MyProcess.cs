﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsServiceTemplate
{
    public class MyProcess : IProcess
    {
        public void Run()
        {
            try
            {
                System.IO.File.WriteAllText("c:\\MyServiceTestLog.txt",
                    string.Format("Service succesfully started at {0}", DateTime.Now));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Stop()
        {
            try
            {
                System.IO.File.AppendAllText("c:\\MyServiceTestLog.txt", 
                    string.Format("\nService succesfully ended at {0}", DateTime.Now));

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
