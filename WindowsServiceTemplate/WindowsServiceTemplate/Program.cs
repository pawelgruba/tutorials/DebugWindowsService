﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace WindowsServiceTemplate
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            if (args.Length > 0 && args[0].ToLower() == "-console")
            {
                try
                {
                    var process = new MyProcess();
                    process.Run();
                    Console.Read();
                    process.Stop();
                    return;

                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                }

            }
            
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new MyServiceHost()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
