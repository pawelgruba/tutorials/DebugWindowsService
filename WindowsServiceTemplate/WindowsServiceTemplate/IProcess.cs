﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsServiceTemplate
{
    public interface IProcess
    {
        void Run();
        void Stop();
    }
}
