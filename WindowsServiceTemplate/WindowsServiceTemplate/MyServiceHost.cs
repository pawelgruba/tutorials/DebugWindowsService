﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace WindowsServiceTemplate
{
    public partial class MyServiceHost : ServiceBase
    {
        private MyProcess _myProcess;

        public MyServiceHost()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            if (_myProcess == null)
                _myProcess = new MyProcess();

            _myProcess.Run();
        }

        protected override void OnStop()
        {
            if (_myProcess == null)
                return;

            _myProcess.Stop();
        }
    }
}
